package org.torproject.artitoyvpn.ui.logging;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import org.torproject.artitoyvpn.utils.Utils;

import java.util.ArrayList;
import java.util.Locale;

public class LogObservable extends ViewModel {

    private final MutableLiveData<ArrayList<LogItem>> logListData;
    private static LogObservable instance;

    private LogObservable() {
        logListData = new MutableLiveData<>(new ArrayList<>());
    }

    public static LogObservable getInstance() {
        if (instance == null) {
            instance = new LogObservable();
        }
        return instance;
    }

    public void addLog(String log) {
        ArrayList<LogItem> list = logListData.getValue();
        if (list != null) {
            list.add(new LogItem(Utils.getFormattedDate(System.currentTimeMillis(), Locale.getDefault()), log));
            logListData.postValue(list);
        }
    }


    public String getLogStrings(boolean showTimestamp) {
        StringBuilder builder = new StringBuilder();
        ArrayList<LogItem> logItemArrayList = logListData.getValue();
        if (logItemArrayList == null) {
            return "";
        }
        for (LogItem item : logItemArrayList) {
            builder.append(item.toString(showTimestamp)).append("\n");
        }
        return builder.toString();
    }

    public LiveData<ArrayList<LogItem>> getLogListData() {
        return logListData;
    }

}