use std::{
    net::IpAddr,
    pin::Pin,
    sync::{Arc, Mutex},
    task::{Context, Poll},
};

use log::info;
use smoltcp::{
    iface::{Interface, SocketHandle},
    phy::TunTapInterface,
};
use tokio::io::{AsyncRead, AsyncWrite, ReadBuf};

use crate::device::VirtualDevice;

type IFace = Arc<Mutex<Interface<'static, VirtualDevice<TunTapInterface>>>>;

pub struct TcpSocket {
    handle: SocketHandle,
    iface: IFace,
}

impl TcpSocket {
    pub fn new(iface: IFace, s: smoltcp::socket::TcpSocket<'static>) -> Self {
        let handle = iface.lock().unwrap().add_socket(s);
        Self { handle, iface }
    }

    fn with<R>(&mut self, f: impl FnOnce(&mut smoltcp::socket::TcpSocket) -> R) -> R {
        f(self
            .iface
            .lock()
            .unwrap()
            .get_socket::<smoltcp::socket::TcpSocket>(self.handle))
    }

    pub fn dest(&mut self) -> (IpAddr, u16) {
        self.with(|s| {
            let endpoint = s.local_endpoint();
            (endpoint.addr.into(), endpoint.port)
        })
    }
}

impl Drop for TcpSocket {
    fn drop(&mut self) {
        self.iface.lock().unwrap().remove_socket(self.handle);
    }
}

impl AsyncRead for TcpSocket {
    fn poll_read(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &mut ReadBuf<'_>,
    ) -> Poll<std::io::Result<()>> {
        self.with(|s| match s.can_recv() {
            true => {
                let rbuf = s.recv(|b| (b.len(), b));
                match rbuf {
                    Err(e) => {
                        return Poll::Ready(Err(std::io::Error::new(
                            std::io::ErrorKind::Other,
                            format!("{}", e),
                        )))
                    }
                    Ok(b) => {
                        if b.len() > 0 {
                            buf.put_slice(b);
                            info!("Async Read: {:?}", buf);
                            Poll::Ready(Ok(()))
                        } else {
                            s.register_recv_waker(cx.waker());
                            Poll::Pending
                        }
                    }
                }
            }
            false => {
                s.register_recv_waker(cx.waker());
                Poll::Pending
            }
        })
    }
}

impl AsyncWrite for TcpSocket {
    fn poll_write(
        mut self: Pin<&mut Self>,
        cx: &mut Context<'_>,
        buf: &[u8],
    ) -> Poll<std::io::Result<usize>> {
        let p = self.with(|s| match s.can_send() {
            true => match s.send_slice(buf) {
                Ok(0) => {
                    s.register_send_waker(cx.waker());
                    Poll::Pending
                }
                Ok(n) => Poll::Ready(Ok(n)),
                Err(e) => Poll::Ready(Err(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    format!("{}", e),
                ))),
            },
            false => {
                s.register_send_waker(cx.waker());
                Poll::Pending
            }
        });
        match p {
            Poll::Ready(_) => {
                // We need to poll in order to process the ingress packets.
                let _ = self
                    .iface
                    .lock()
                    .unwrap()
                    .poll(smoltcp::time::Instant::now());
            }
            _ => (),
        };
        p
    }

    fn poll_flush(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<std::io::Result<()>> {
        Poll::Ready(Ok(()))
    }

    fn poll_shutdown(mut self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<std::io::Result<()>> {
        // XXX: Maybe should be close() and then check the state?
        self.with(|s| s.abort());
        Poll::Ready(Ok(()))
    }
}
