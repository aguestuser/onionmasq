mod device;
mod parser;
mod proxy;
mod socket;

use arti_client::{TorClient, TorClientConfig};
use device::VirtualDevice;
use log::{debug, info};
use proxy::ArtiProxy;
use smoltcp::{
    iface::{Interface, InterfaceBuilder, Routes},
    phy::{Medium, TunTapInterface},
    wire::{IpAddress, IpCidr, Ipv4Address},
};
use std::{
    collections::BTreeMap,
    sync::{Arc, Mutex},
};
use tor_rtcompat::tokio::TokioNativeTlsRuntime;

use crate::{parser::Parser, socket::TcpSocket};

pub struct OnionTunnel {
    iface_name: String,
    iface: Arc<Mutex<Interface<'static, VirtualDevice<TunTapInterface>>>>,
    arti: TorClient<TokioNativeTlsRuntime>,
}

impl OnionTunnel {
    pub async fn new(iface_name: &str) -> Self {
        let tun = TunTapInterface::new(iface_name, Medium::Ip).expect("Unable to create TUN iface");
        let device = VirtualDevice::new(tun);

        // Create our iface.
        let mut routes = Routes::new(BTreeMap::new());
        routes
            .add_default_ipv4_route(Ipv4Address::new(0, 0, 0, 1))
            .expect("Unable to add default IPv4 route");
        let iface_builder = InterfaceBuilder::new(device, Vec::new())
            .ip_addrs([IpCidr::new(IpAddress::v4(0, 0, 0, 1), 0)])
            .routes(routes)
            .any_ip(true);

        let config = TorClientConfig::default();
        let rt: TokioNativeTlsRuntime = tokio::runtime::Handle::current().into();
        let arti =
            TorClient::create_unbootstrapped(rt, config, arti_client::BootstrapBehavior::OnDemand)
                .expect("Unable to create Tor Client");

        info!("Arti bootstrap finalized.");

        Self {
            iface_name: iface_name.to_string(),
            iface: Arc::new(Mutex::new(iface_builder.finalize())),
            arti,
        }
    }

    fn iface_poll(&self, timestamp: smoltcp::time::Instant) {
        match self.iface.lock().unwrap().poll(timestamp) {
            Ok(_) | Err(smoltcp::Error::Exhausted) => {}
            Err(error) => {
                debug!("Poll error: {}", error)
            }
        }
    }

    fn proxy(&mut self, socket: TcpSocket) {
        let mut proxy = ArtiProxy::new(socket, self.arti.clone());
        tokio::spawn(async move { proxy.start().await });
    }

    pub async fn start(&mut self) {
        info!("Starting onion tunnel on TUN iface {}", self.iface_name);

        loop {
            let timestamp = smoltcp::time::Instant::now();

            // The first poll we queue packets in the receive queue.
            self.iface_poll(timestamp);

            let mut packets = self.iface.lock().unwrap().device_mut().take_recv_queue();

            // Handle incoming packet. Drain packets as we process them.
            while let Some(packet) = packets.pop_front() {
                if let Some(tcp_socket) = Parser::parse(packet.clone()).take() {
                    let socket = TcpSocket::new(self.iface.clone(), tcp_socket);
                    self.proxy(socket);
                }
                self.iface.lock().unwrap().device_mut().put_packet(packet);
            }

            // The second poll we do process packets in the receive queue from the first poll.
            self.iface_poll(timestamp);

            let fd = self.iface.lock().unwrap().device().as_raw_fd();
            /*
            //let fd = self.iface.lock().unwrap().device().as_raw_fd();
            //let async_fd = AsyncFd::new(fd).unwrap();
            let iface = self.iface.lock().unwrap();
            let async_fd = iface.device().device();

            let timeout = if let Some(delay) = iface.poll_delay(timestamp) {
                delay
            } else {
                smoltcp::time::Duration::from_secs(0)
            };

            let _ = tokio::time::timeout(timeout.into(), async_fd.readable()).await;
            */
            let timeout = self.iface.lock().unwrap().poll_delay(timestamp);
            smoltcp::phy::wait(fd, timeout).expect("wait error");
        }
    }
}
