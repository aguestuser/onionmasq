// Copyright (c) 2021, The Tor Project, Inc.
// See LICENSE for licensing information.

use std::collections::VecDeque;
use std::os::unix::prelude::{AsRawFd, RawFd};

use smoltcp::phy::{Device, DeviceCapabilities, RxToken as SmolRxToken};
use smoltcp::time::Instant;
use tokio::io::unix::AsyncFd;

pub type Packet = Vec<u8>;

#[derive(Copy, Clone)]
enum DevicePollState {
    Processing,
    Queuing,
}

pub struct VirtualDevice<D>
where
    D: for<'a> smoltcp::phy::Device<'a> + AsRawFd,
{
    device: AsyncFd<D>,
    recv_queue: VecDeque<Packet>,
    poll_mode: DevicePollState,
}

impl<'a, D> VirtualDevice<D>
where
    D: for<'d> smoltcp::phy::Device<'d> + AsRawFd,
{
    pub fn new(device: D) -> Self {
        Self {
            device: AsyncFd::new(device).unwrap(),
            recv_queue: VecDeque::new(),
            poll_mode: DevicePollState::Queuing,
        }
    }

    pub fn _device(&self) -> &AsyncFd<D> {
        &self.device
    }

    pub fn as_raw_fd(&self) -> RawFd {
        self.device.get_ref().as_raw_fd()
    }

    pub fn take_recv_queue(&mut self) -> VecDeque<Packet> {
        std::mem::take(&mut self.recv_queue)
    }

    pub fn put_packet(&mut self, packet: Packet) {
        self.recv_queue.push_back(packet);
    }

    fn queue(
        &'a mut self,
    ) -> Option<(
        <VirtualDevice<D> as Device<'a>>::RxToken,
        <VirtualDevice<D> as Device<'a>>::TxToken,
    )> {
        // Next poll state after we've queued.
        self.poll_mode = DevicePollState::Processing;

        // We receive packets from our internal device and discard the Tx-token since we can
        // regenerate that from the transmit() method later.
        while let Some((rx_token, _tx_token)) = self.device.get_mut().receive() {
            // We consume the Rx-token to extract the packet content for inspection. The call to
            // unwrap() should be safe here since we return Ok(...) explicitly.
            let packet = rx_token
                .consume(Instant::now(), |packet| Ok(Packet::from(packet)))
                .unwrap();

            // Append the packet to our queue.
            self.recv_queue.push_back(packet);
        }
        // Always return None here so we can process the packet and turn them into TCP sockets if
        // need be to proxy to arti.
        None
    }

    fn process(
        &'a mut self,
    ) -> Option<(
        <VirtualDevice<D> as Device<'a>>::RxToken,
        <VirtualDevice<D> as Device<'a>>::TxToken,
    )> {
        // Next poll state after we've processed, we go back in Queuing.
        self.poll_mode = DevicePollState::Queuing;

        self.recv_queue.pop_front().map(|packet| {
            let device_tx_token = self.device.get_mut().transmit().unwrap();

            let rx_token = RxToken { packet };
            let tx_token = TxToken {
                lower: device_tx_token,
            };

            (rx_token, tx_token)
        })
    }
}

#[derive(Debug)]
pub struct RxToken {
    packet: Packet,
}

impl smoltcp::phy::RxToken for RxToken {
    fn consume<R, F>(mut self, _timestamp: Instant, f: F) -> smoltcp::Result<R>
    where
        F: FnOnce(&mut [u8]) -> smoltcp::Result<R>,
    {
        f(&mut self.packet)
    }
}

pub struct TxToken<Tx: smoltcp::phy::TxToken> {
    lower: Tx,
}

impl<Tx: smoltcp::phy::TxToken> smoltcp::phy::TxToken for TxToken<Tx> {
    fn consume<R, F>(self, timestamp: Instant, len: usize, f: F) -> smoltcp::Result<R>
    where
        F: FnOnce(&mut [u8]) -> smoltcp::Result<R>,
    {
        self.lower.consume(timestamp, len, f)
    }
}

impl<'a, D> smoltcp::phy::Device<'a> for VirtualDevice<D>
where
    D: for<'d> smoltcp::phy::Device<'d> + AsRawFd,
{
    type RxToken = RxToken;
    type TxToken = TxToken<<D as Device<'a>>::TxToken>;

    fn capabilities(&self) -> DeviceCapabilities {
        self.device.get_ref().capabilities()
    }

    fn receive(&'a mut self) -> Option<(Self::RxToken, Self::TxToken)> {
        match self.poll_mode {
            DevicePollState::Queuing => self.queue(),
            DevicePollState::Processing => self.process(),
        }
    }

    fn transmit(&'a mut self) -> Option<Self::TxToken> {
        self.device
            .get_mut()
            .transmit()
            .map(|lower| TxToken { lower })
    }
}
