use arti_client::DangerouslyIntoTorAddr;
use arti_client::TorClient;
use log::info;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tor_rtcompat::tokio::TokioNativeTlsRuntime;

use crate::socket::TcpSocket;

pub struct ArtiProxy {
    socket: TcpSocket,
    arti: TorClient<TokioNativeTlsRuntime>,
}

impl ArtiProxy {
    pub fn new(socket: TcpSocket, arti: TorClient<TokioNativeTlsRuntime>) -> Self {
        Self { socket, arti }
    }

    pub async fn start(&mut self) {
        info!("Starting Arti Proxy");

        let dest = self.socket.dest();
        info!("Connecting to: {:?}", dest);
        let mut arti_stream = self
            .arti
            .connect(dest.into_tor_addr_dangerously().unwrap())
            .await
            .unwrap();
        info!("Connected to: {:?}", dest);

        loop {
            let mut arti_buf = Vec::new();
            let mut tun_buf = Vec::new();

            tokio::select! {
                r = self.socket.read_buf(&mut tun_buf) => match r {
                    Ok(n) => {
                        if n > 0 {
                            let ret = arti_stream.write_all(tun_buf.as_slice()).await;
                            let _ = arti_stream.flush().await;
                            info!("Write to arti: {:?}", ret);
                        }
                    }
                    Err(_) => break,
                },
                r = arti_stream.read_buf(&mut arti_buf) => match r {
                    Ok(n) => {
                        if n > 0 {
                            let ret = self.socket.write(arti_buf.as_slice()).await;
                            info!("Write to onioni0: {:?}", ret);
                        }
                    }
                    Err(_) => break,
                }
            };
        }
    }
}
